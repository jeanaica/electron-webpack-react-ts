Basic boilerplate for a portable electron app setup on windows.

This boilerplate has eslint, typescript, prettier, and husky.

To build: `yarn build`

To create a portable package: `yarn dist`

For development: `yarn start`
